vscode-rem-old-bins()
{
  ls -dtr ~/.vscode-server/bin/*/ | head --lines=-1 | xargs --no-run-if-empty rm -rv
}

list-sourced-files()
{
  /bin/bash -lixc exit 2>&1 | sed -n 's/^+* \(source\|\.\) //p'
}

apache_error_log_less()
{
  grep  "PHP message" /opt/bitnami/apache/logs/error_log |less -N
}

apache_error_log_tail()
{
  tail -f /opt/bitnami/apache/logs/error_log | grep "PHP message"
}

list_unique_ext() { 
  find . -type f | awk -F. '!a[$NF]++{print $NF}' 
}
