fdel(){ find -iname "*$1*" -delete ; }
fbr(){ find -iname "*$1*" -exec mp3info -r a -p "%f %r\n" {} \; ; }

sd-mount()
{
	sudo mkdir /mnt/d
	sudo mount -t drvfs D: /mnt/d
}

sd-umount()
{
	sudo umount /mnt/d
}

