alias gg="git log --graph --pretty=format:'%C(auto)%h%d %s %Cgreen%aN %Cblue%ar'"
alias ggl="gg --branches"
alias gga="gg --all"
alias ggs="gg --simplify-by-decoration"
#alias gg='git log --graph --decorate --color --pretty=oneline --abbrev-commit'
#alias gga='git log --graph --decorate --color --pretty=oneline --abbrev-commit --all'
alias gg7="gg --since='7 days ago' --author='$USER'"
alias gg30="gg --since='30 days ago' --author='$USER'"

alias gs='git status'
alias gsw='git show --color-words="\w+|\S"'
alias gh='git show '

alias gd='git diff --color'
alias gdw='git diff --color-words="\w+|\S"'
alias gds='gd --staged'
alias gdsw='gdw --cached'
alias gdwn='git diff -U0 -w --no-color'
alias gnows='gdwn | git apply --cached --ignore-whitespace --reject --whitespace=fix --unidiff-zero -'

alias gst='git --no-pager stash '
alias gstku='gst -k -u'

alias gsp='gst push -- '
alias gsm='gst push -m '

alias gb='git branch --sort=-committerdate'
alias gmt='git mergetool'
alias gcor='find -name "*orig" -delete'
alias gan='git commit --amend --no-edit'

alias phpl='for f in $(git ls-files -m); do php -l $f; done'

ggr()
{
	gg --remotes=$1
}

#git config --global merge.tool kdiff3
git config --global mergetool.keepBackup false
git config --global mergetool.keepTemporaries false

git config --global rebase.updateRefs true

git config --global core.editor "vim"

git config --global core.pager "less -+FX"
git config --global pager.diff "less -+FX"
git config --global pager.show "less -+FX"
git config --global pager.log "less -+FX"
git config --global pager.reflog "less -+FX"
git config --global pager.grep "less -+FX"
git config --global pager.branch 'less -+FX'

#git config --get-regexp alias
git config --global alias.tv "tag --sort=v:refname"
git config --global alias.pr "pull -r origin master"
git config --global alias.pf "pull --ff-only origin master"
git config --global alias.ri "rebase -i origin/master"
git config --global alias.rl "reflog --date=relative"
git config --global alias.wl "worktree list"
git config --global alias.sl "stash list"

#If that's the first time you checkout a repo you need to use --init first
git config --global alias.sui "submodule update --init --recursive"
git config --global alias.sur "submodule update --recursive --remote"
git config --global push.recurseSubmodules on-demand

gss()
{
	git stash show -p stash@{$1}
}

gsa()
{
	git stash apply "stash@{$1}"
}

gsas()
{
	STASH=$1
	shift
	git checkout "stash@{$STASH}" -- $*
}

gsd()
{
	gst drop stash@{$1}
}

push-fork()
{
	 git push -f fork $(git rev-parse HEAD):master
}

push-master()
{
	 git push origin $(git rev-parse HEAD):master
}


# git prompt configuration
GIT_PS1_SHOWUPSTREAM=auto
GIT_PS1_SHOWCOLORHINTS=1
GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWUNTRACKEDFILES=1
GIT_PS1_STATESEPARATOR=' '
export PROMPT_COMMAND='__git_ps1 "\w" " > "'

return

#ubuntu: sourcing "call stack"
/home/phanzel/.bashrc
/usr/share/bash-completion/bash_completion
/etc/bash_completion.d/git-prompt
/usr/lib/git-core/git-sh-prompt
